import pygame, sys

BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)

XBOX_BUTTONS = ["A", "B", "X", "Y", "LT", "RT", "SELECT", "MENU", "LEFT JOYSTICK", "RIGHT JOYSTICK"]
XBOX_AXES = ["LEFT JOYSTICK X AXIS", "LEFT JOYSTICK Y AXIS", "BUMPERS", "RIGHT JOYSTICK X AXIS", "RIGHT JOYSTICK Y AXIS"]
XBOX_HAT_X = ["LEFT", "", "RIGHT"]
XBOX_HAT_Y = ["DOWN", "", "UP"]

PS3_BUTTONS = ["X", "O", "SQUARE", "TRIANGLE", "LT", "RT", "SELECT", "START", "LEFT JOYSTICK", "RIGHT JOYSTICK"]
PS3_AXES = ["LEFT JOYSTICK X AXIS", "LEFT JOYSTICK Y AXIS", "BUMPERS", "RIGHT JOYSTICK Y AXIS", "RIGHT JOYSTICK X AXIS"]
PS3_HAT_X = ["LEFT", "", "RIGHT"]
PS3_HAT_Y = ["DOWN", "", "UP"]


BUTTONS = PS3_BUTTONS
AXES = PS3_AXES
HATS_X = PS3_HAT_X
HATS_Y = PS3_HAT_Y

class ControllerApp(object):
    def __init__(self):
        """Set up joystick and screen display

        Constructor: ControllerApp(None)
        """
        
        pygame.init()

        self._js = self.find_joystick()
        if self._js == None:
            return

        self._js.init()
        self.store_info()

        size = [500, 400]
        self._screen = pygame.display.set_mode(size)

        pygame.display.set_caption("Controller state")#"State for {}".format(self._js.get_name()))
        self._textPrint = TextPrint()

        self.run()

    def clear_screen(self):
        """ Clear display

        ControllerApp.clear_screen(None) -> None
        """
        self._screen.fill(WHITE)
        self._textPrint.reset()
    

    def run(self):
        """ Clear display, find and display state of controller on screen

        ControllerApp.run(None) -> None
        """
        while True:
            self.clear_screen()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
            self.check_buttons()
            self.check_axes()
            self.check_hats()
            
            pygame.display.flip()
            
        

    def find_joystick(self):
        """ Discover any connected joysticks. Returns first one found

        ControllerApp.find_joystick(None) -> pygame.Joystick
        """
        joystick_count = pygame.joystick.get_count()
        print("Joystick(s) found:", joystick_count)

        if joystick_count == 0:
            print("Error: no joystick detected")
            pygame.quit()
            return

        return pygame.joystick.Joystick(0)

    def store_info(self):
        """ Find info for joystick, and set instance values

        ControllerApp.store_info(None) -> None
        """
        self._axes = self._js.get_numaxes()
        self._buttons = self._js.get_numbuttons()
        self._hats = self._js.get_numhats()

        print("There are", self._axes, "axes")
        print("There are", self._buttons, "button(s)")
        print("There are", self._hats, "hat(s)")

    def check_buttons(self):
        """ Query state of buttons

        ControllerApp.check_buttons(None) -> None
        """
        for i in range(self._buttons):
            self._textPrint.print(self._screen, "{}: {}".format(BUTTONS[i], self._js.get_button(i)))

    def check_axes(self):
        """ Query state of axes

        ControllerApp.check_axes(None) -> None
        """
        for i in range(self._axes):
            self._textPrint.print(self._screen, "{}: {:.4f}".format(AXES[i], self._js.get_axis(i)))

    def check_hats(self):
        """ Query state of hats

        ControllerApp.check_hats(None) -> None
        """
        for i in range(self._hats):
            x, y = self._js.get_hat(i)
            self._textPrint.print(self._screen, "Arrow pad - Horizontal: {}".format(HATS_X[x + 1]))
            self._textPrint.print(self._screen, "Arrow pad - Vertical: {}".format(HATS_Y[y + 1]))



### Class to print text to PyGame screen
### To be documented soon. Very self-explanatory
class TextPrint:
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def print(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, [self.x, self.y])
        self.y += self.line_height
        
    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15
        
    def indent(self):
        self.x += 10
        
    def unindent(self):
        self.x -= 10
        
def main():
    app = ControllerApp()
    

if __name__ == "__main__":
    main()
