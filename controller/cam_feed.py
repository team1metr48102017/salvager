import pygame
import pygame.camera
from pygame.locals import *

class CaptureCamera(object):
    def __init__(self):
        self._size = (640, 480)
        self._display = pygame.display.set_mode(self._size, 0)

        camlist = pygame.camera.list_cameras()
        if camlist:
            self._cam = pygame.camera.Camera(camlist[0], (640, 480))

        self._cam.start()

        self._screen = pygame.surface.Surface(self._size, self._display)

    def get_image(self):
        if self._cam.query_image():
            self._screen = self._cam.get_image(self._screen)

        self._display.blit(self._screen, (0, 0))

    def get_cam(self):
        return self._cam

    def stop(self):
        self._cam.stop()

def main():
    pygame.init()
    pygame.camera.init()

    cap_cam = CaptureCamera()
    while True:
        for e in pygame.event.get():
            if e.type == QUIT:
                cap_cam.stop()
                break
        cam_cam.get_image()

if __name__ == "__main__":
    main()
