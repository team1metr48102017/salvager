import pygame, sys
import serial

BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
MAX_DUTY_CYCLE = 99

XBOX_BUTTONS = ["A", "B", "X", "Y", "LT", "RT", "SELECT", "MENU", "LEFT JOYSTICK", "RIGHT JOYSTICK"]
XBOX_AXES = ["LEFT JOYSTICK X AXIS", "LEFT JOYSTICK Y AXIS", "BUMPERS", "RIGHT JOYSTICK X AXIS", "RIGHT JOYSTICK Y AXIS"]
XBOX_HAT_X = ["LEFT", "", "RIGHT"]
XBOX_HAT_Y = ["DOWN", "", "UP"]

PS3_BUTTONS = ["X", "O", "SQUARE", "TRIANGLE", "LB", "RB", "SELECT", "START", "LEFT JOYSTICK", "RIGHT JOYSTICK"]
PS3_AXES = ["LX", "LY", "TRIGGERS", "RY", "RX"]
PS3_HAT_X = ["LEFT", "", "RIGHT"]
PS3_HAT_Y = ["DOWN", "", "UP"]

##BUTTON_PRESS = ["C80", "C20", "", "", "W20", "T20", "", "", "", ""]
##BUTTON_RELEASE = ["C50", "C50", "", "", "W50", "T50", "", "", "", ""]

#motor DC set values for button-controlled things
HIGH = "79"
STOP = "49"
LOW = "19"

MIN_AXIS_DELTA = 1
AXIS_INACTIVE_THRESHHOLD = 0.1
AXIS_BUTTON_THRESHHOLD = 0.8

COMMANDS = {"X": ["C"+STOP, "C"+HIGH],\
            "O": ["C"+STOP, "C"+LOW],\
            "RB": ["T"+STOP, "T"+LOW],\
            "RT": ["T"+STOP, "T"+HIGH],\
            "LB": ["W"+STOP, "W"+HIGH],\
            "LT": ["W"+STOP, "W"+LOW],\
            "LY": "L",
            "RY": "R"}

BUTTONS = PS3_BUTTONS
AXES = PS3_AXES
HATS_X = PS3_HAT_X
HATS_Y = PS3_HAT_Y

class ControllerApp(object):
    def __init__(self):
        """Set up joystick and screen display

        Constructor: ControllerApp(None)
        """
        
        pygame.init()

        self._js = self.find_joystick()
        if self._js == None:
            return

        self._js.init()
        self._button_state = [0 for i in BUTTONS] #set buttons to off initially
        self._trigger_state = [0, 0] # set triggers RT and LT to off initially
        self._prop_state = [49, 49] # set prop axes to off initially
        self.store_info()

        size = [500, 400]
        self._screen = pygame.display.set_mode(size)

        pygame.display.set_caption("Controller state")#"State for {}".format(self._js.get_name()))
        self._textPrint = TextPrint()

        self._ser = SerialCommand("COM6", 9600)

        self.run()

    def clear_screen(self):
        """ Clear display

        ControllerApp.clear_screen(None) -> None
        """
        self._screen.fill(WHITE)
        self._textPrint.reset()
    

    def run(self):
        """ Clear display, find and display state of controller on screen

        ControllerApp.run(None) -> None
        """
        while True:
            self.clear_screen()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
            self.check_buttons()
            self.check_axes()
            self.check_hats()
            
            pygame.display.flip()
            
        

    def find_joystick(self):
        """ Discover any connected joysticks. Returns first one found

        ControllerApp.find_joystick(None) -> pygame.Joystick
        """
        joystick_count = pygame.joystick.get_count()
        print("Joystick(s) found:", joystick_count)

        if joystick_count == 0:
            print("Error: no joystick detected")
            pygame.quit()
            return

        return pygame.joystick.Joystick(0)

    def store_info(self):
        """ Find info for joystick, and set instance values

        ControllerApp.store_info(None) -> None
        """
        self._axes = self._js.get_numaxes()
        self._buttons = self._js.get_numbuttons()
        self._hats = self._js.get_numhats()

        print("There are", self._axes, "axes")
        print("There are", self._buttons, "button(s)")
        print("There are", self._hats, "hat(s)")

    def check_buttons(self):
        """ Query state of buttons

        ControllerApp.check_buttons(None) -> None
        """
        for i in range(self._buttons):
            b = BUTTONS[i]
            state = self._js.get_button(i)
            
            self._textPrint.print(self._screen, "{}: {}".format(b, state))

            if b in COMMANDS.keys() and self.button_changed_state(i, state):
                self._ser.send(COMMANDS.get(BUTTONS[i])[state])
                
            self._button_state[i] = state

    def button_changed_state(self, i, state):
        return (self.button_rising_edge(i, state) or self.button_falling_edge(i, state))

    def check_axes(self):
        """ Query state of axes

        ControllerApp.check_axes(None) -> None
        """
        for i in range(self._axes):
            a = AXES[i]
            val = self._js.get_axis(i)
            self._textPrint.print(self._screen, "{}: {:.4f}".format(a, val))
            if a == "TRIGGERS":
                self.triggers_to_buttons(val)
            else:
                self.convert_axis(i, val)

    def check_hats(self):
        """ Query state of hats

        ControllerApp.check_hats(None) -> None
        """
        for i in range(self._hats):
            x, y = self._js.get_hat(i)
            self._textPrint.print(self._screen, "Arrow pad - Horizontal: {}".format(HATS_X[x + 1]))
            self._textPrint.print(self._screen, "Arrow pad - Vertical: {}".format(HATS_Y[y + 1]))

    def button_changed_state(self, i, state):
        return (self.button_rising_edge(i, state) or self.button_falling_edge(i, state))

    def button_rising_edge(self, i, new_state):
        if not self._button_state[i] and new_state:
            return True
        return False

    def button_falling_edge(self, i, new_state):
        if self._button_state[i] and not new_state:
            return True
        return False

    def trigger_changed_state(self, i, state):
        return (self.trigger_rising_edge(i, state) or self.trigger_falling_edge(i, state))

    def trigger_rising_edge(self, i, new_state):
        if not self._trigger_state[i] and new_state:
            return True
        return False

    def trigger_falling_edge(self, i, new_state):
        if self._trigger_state[i] and not new_state:
            return True
        return False

    def convert_axis(self, i, val):
        axis = AXES[i]
        if axis not in COMMANDS:
            return

        if axis == "RY":
            i = 0
        else:
            i = 1

        dc = self._prop_state[i]

        if abs(val) < AXIS_INACTIVE_THRESHHOLD and dc == 49:
            return
        
        motor = COMMANDS[axis]

        duty = round((val * MAX_DUTY_CYCLE + MAX_DUTY_CYCLE) / 2)
        
        if abs(duty - dc) >= MIN_AXIS_DELTA:
            self._ser.send(motor + str(duty))
            self._prop_state[i] = duty


    def triggers_to_buttons(self, val):
        if val <= 0:
            i = 0
            button = "RT"
        else:
            i = 1
            button = "LT"
            
        state = 0
        if abs(val) >= AXIS_BUTTON_THRESHHOLD:
            state = 1

        if button in COMMANDS.keys() and self.trigger_changed_state(i, state):
            self._ser.send(COMMANDS.get(button)[state])
                
        self._trigger_state[i] = state

        
            
            

### Class to print text to PyGame screen
### To be documented soon. Very self-explanatory
class TextPrint:
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def print(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, [self.x, self.y])
        self.y += self.line_height
        
    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15
        
    def indent(self):
        self.x += 10
        
    def unindent(self):
        self.x -= 10

class SerialCommand(object):
    def __init__(self, port, baud_rate, to=0):
        self._ser = serial.Serial(port, baud_rate, timeout=to)

        if not self._ser.isOpen():
            print("Could not open serial connection.")
            return

        self.send("XXX")

    def send(self, message):
        line = message + "\r\n"
        self._ser.write(line.encode())
        
def main():
    app = ControllerApp()
    

if __name__ == "__main__":
    main()
