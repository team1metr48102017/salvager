import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial("COM6", 9600, timeout=0)
print(ser.portstr)
ser.isOpen()

print('Enter your commands below.\r\nInsert "exit" to leave the application.')

ser.write('hello'.encode())
while True:
    # get keyboard input
    user_input = input("Input: ")
        # Python 3 users
        # input = input(">> ")
    if user_input == 'exit':
        ser.close()
        exit()
    else:
        # send the character to the device
        # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
        ser.write(user_input.encode())
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:
            out += ser.read(1)

        if out != '':
            print(">>" + out)
